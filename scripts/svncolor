#!/usr/bin/env python

"""
Author: Saophalkun Ponlu (http://phalkunz.com)
Contact: phalkunz@gmail.com
Date: May 23, 2009
Modified: June 15, 2009

Additional modifications:
Author: Phil Christensen (http://bubblehouse.org)
Contact: phil@bubblehouse.org
Date: February 22, 2010

Author: Huangshan Chen
Date: 2019-04-16, Tue
Note: change the print out to a pager version
"""

import sys, subprocess, tempfile, os

colorizedSubcommands = (
    'status',
    'stat',
    'st',
    'add',
    'remove',
    'diff',
    'di',
    'log',
)

statusColors = {
    "-"     : "31",     # red, diff output
    "+"     : "32",     # green
    "@@"    : "36",     # blue
    "M "    : "31",     # red, status on file
    "? "    : "36",     # Cyan
    "A "    : "32",     # green
    "X "    : "33",     # yellow
    "C "    : "30;41",  # black on red
    "D "    : "31;1",   # bold red
    " M"    : "31",     # red, status on directory
    " C"    : "30;41",     # black on red
}

def colorize(line):
    for status in statusColors:
        if line.startswith(status):
            return ''.join(("\033[", statusColors[status], "m", line, "\033[m"))
    else:
        return line

pager = True
if __name__ == '__main__':
    command = sys.argv
    command[0] = 'svn'
    # command[0] = '/common/run/unix/svn/1.7.9/Linux/x86_64/bin/svn' # svn version 1.7.9

    if len(command) > 1:
        subcommand = (command[1], '')[len(command) < 2]
    else:
        subcommand = ''
    if subcommand in colorizedSubcommands and sys.stdout.isatty() and pager:
        tmp_file = open(tempfile.mkstemp()[1],'w')
        task = subprocess.Popen(command, stdout=subprocess.PIPE)
        while True:
            line = task.stdout.readline()
            if not line:
                break
            tmp_file.write(colorize(line))
        tmp_file.flush()
        subprocess.call(['less', '-FXR', tmp_file.name])
        tmp_file.close()
    else:
        task = subprocess.Popen(command)
    task.communicate()
    sys.exit(task.returncode)
